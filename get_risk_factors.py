import argparse
import json
from tqdm import tqdm

from infrastructure.cause_splitter import CauseSplitter

parser = argparse.ArgumentParser()
parser.add_argument("-m",dest="model_name", type=str)
parser.add_argument("-r",dest="seed", default=41, type=int)
parser.add_argument("-e",dest="experiment_name", default=41, type=str)

args = parser.parse_args()
seed = args.seed
model_name = args.model_name
experiment_name = args.experiment_name

test_name = "replicability_model-{}_experiement-{}_seed-{}".format(model_name, experiment_name, seed)
attention_file_path = "model_output/attention/{}.json".format(test_name)

splitter = CauseSplitter(.50)
important_word = {
    splitter.ALCOHOL: [],
    splitter.MEDICATION: [],
    splitter.PRESCRIPTION: [],
    splitter.NARCOTIC: []
}

important_word_with_attention = {
    splitter.ALCOHOL: [],
    splitter.MEDICATION: [],
    splitter.PRESCRIPTION: [],
    splitter.NARCOTIC: []
}

attention_threshold = 0.50

attention_file = json.load(open(attention_file_path, 'r'))
paragraphs = {}

for sentence in tqdm(attention_file, total=len(attention_file)):
    sentence_paragraph = "-".join(sentence['id'].split("-")[:-1])
    if sentence_paragraph not in paragraphs:
        paragraphs[sentence_paragraph] = []
    paragraphs[sentence_paragraph].append(sentence)

for case_paragraph in tqdm(paragraphs.values(),total=len(paragraphs)):
    paragraph_has_intoxicant = bool(sum([sentence['prediction'] for sentence in case_paragraph]))
    if paragraph_has_intoxicant:
        sentences_attention = [sentence['text'][1] for sentence in case_paragraph]
        maximum_attention = float(max([sentence['text'][1] for sentence in case_paragraph]))
        content = set()
        for sentence_data,sentence_attention in zip(case_paragraph,sentences_attention):
            tokenized_text = sentence_data['text'][3:]
            sentence = {
                'word_attention':sentence_data['attention'][3:len(tokenized_text)],
                'tokenized_text':tokenized_text,
                'attention': float(sentence_attention),
                'inference_certainty':sentence_data['posterior'][-1],
                'text': " ".join(sentence_data['text'][3:len(tokenized_text)])
            }
            content = splitter.get_sentence_content(sentence, maximum_attention)
            if len(content) == 1:
                intoxicant = content.pop()
                minimum_word_attention = max(sentence['word_attention']) * attention_threshold
                for (word, attention) in [(word, attention) for word, attention in
                                          zip(sentence['tokenized_text'], sentence['word_attention']) if
                                          attention > minimum_word_attention]:
                    important_word[intoxicant].append(word)
                    # important_word_with_attention[intoxicant].append((word, attention))

from collections import Counter

for intoxicant, important_words in important_word.items():
    risk_factors = Counter(important_words)
    print("~ Intoxicant: {}".format(intoxicant))
    for important_word in risk_factors.most_common(10):
        print("\t - {}".format(important_word))




