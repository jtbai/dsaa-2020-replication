import spacy
import numpy as np

import os
os.environ["SPACY_WARNING_IGNORE"] = "W008"


class CauseSplitter:

    def __init__(self, sentence_attention_tolerance):
        self.nlp = spacy.load('en_core_web_lg')

        self.NARCOTIC = "NARCOTIC"
        self.PRESCRIPTION = "PRESCRIPTION"
        self.MEDICATION = "MEDICATION"
        self.ALCOHOL = "ALCOHOL"

        self.ALCOHOL_VECTOR = self.nlp("alcohol beer wine")
        self.MEDICATION_VECTOR = self.nlp("medication medications antihistamines")
        self.PRESCRIPTION_VECTOR = self.nlp("prescription prescriptions antidepressants")
        self.NARCOTIC_VECTOR = self.nlp("narcotic heroin marijuana cocaine")

        self.SENTENCE_ATTENTION_TOLERANCE = sentence_attention_tolerance
        self.INFERENCE_MINUMUM_CERTAINTY = 0.5

    def compute_intoxifiant_similarity_by_word(self, sentence):
        analysed_sentence = self.nlp(sentence['text'])
        vector_to_compare = [self.ALCOHOL_VECTOR, self.MEDICATION_VECTOR, self.PRESCRIPTION_VECTOR, self.NARCOTIC_VECTOR]
        similarity_by_word_matrix = np.zeros((len(vector_to_compare), len(analysed_sentence)))
        for token_id, token in enumerate(analysed_sentence):
            if str(token) != " " and str(token) != "":
                for base_vector_id, base_vector in enumerate(vector_to_compare):
                    similarity_by_word_matrix[base_vector_id][token_id] = token.similarity(base_vector)

        return similarity_by_word_matrix

    def consider_sentence_for_extraction(self, sentence, maximum_attention):
        if sentence['attention'] >= self.SENTENCE_ATTENTION_TOLERANCE * maximum_attention and sentence[
            'inference_certainty'] >= self.INFERENCE_MINUMUM_CERTAINTY: # :and sentence['alcohol_drug_presence']:
            return True
        return False

    def alcohol_or_medication_or_narcotics(self, sentence):
        alcohol_similarity, medication_similarity, prescription_similarity, narcotic_similarity = self.get_intoxifiant_similarity_weighted_by_word_attention(
            sentence)
        if alcohol_similarity >= max(prescription_similarity, medication_similarity, narcotic_similarity):
            return self.ALCOHOL
        elif medication_similarity >= max(alcohol_similarity, prescription_similarity, narcotic_similarity):
            return self.MEDICATION
        elif prescription_similarity >= max(alcohol_similarity, medication_similarity, narcotic_similarity):
            return self.PRESCRIPTION
        elif narcotic_similarity >= max(alcohol_similarity, medication_similarity, prescription_similarity):
            return self.NARCOTIC

    def get_intoxifiant_similarity_by_sentence(self, sentence):
        alcohol_similarity = self.nlp(sentence['text']).similarity(self.ALCOHOL_VECTOR)
        medication_similarity = self.nlp(sentence['text']).similarity(self.MEDICATION_VECTOR)
        narcotics_similarity = self.nlp(sentence['text']).similarity(self.NARCOTIC_VECTOR)

        return alcohol_similarity, medication_similarity, narcotics_similarity

    def get_intoxifiant_similarity_weighted_by_word_attention(self, sentence):
        similarity_matrix = self.compute_intoxifiant_similarity_by_word(sentence)  # nb_base_vector_comparison x nb_words
        weighted_similarity_by_words = np.multiply(similarity_matrix, np.array(sentence['word_attention']))
        similarity_to_base_vector = np.sum(weighted_similarity_by_words, axis=1)

        return similarity_to_base_vector[0], similarity_to_base_vector[1], similarity_to_base_vector[2], \
               similarity_to_base_vector[3]

    def get_sentence_content(self, sentence, maximum_attention):
        content = set()
        if self.consider_sentence_for_extraction(sentence, maximum_attention):
            content.add(self.alcohol_or_medication_or_narcotics(sentence))
        return content
