# Replicability of the experimentation

## Items to replicate
* Obtain figures from table VI, VII, VIII
* Obtain t-statistics and p-values for experiments
    1. Proposed model vs Baseline model 
    1. Proposed model at 15% of train data vs Baseline at 100% data
    1. Proposed model simultaneous transfer learning vs Sequential Transfer learning
* Train baseline model and our model
* Obtain the risk factors for a specific model
* Visualize the attention weights

## 0. Prerequisite
1. Python 3.7.x
1. A GPU of 4Go
1. Cuda 10.1
1. Git large file system extension (git-lfs)
    * https://packagecloud.io/github/git-lfs/install#bash-deb 
1. A copy of the repository
    * https://anonymous.4open.science/repository/dsaa-2020/
    * This includes code and datasets
1. Installed packages from requirements.txt
    * ``` pip install -r requirements.txt ```
1. SpaCy English word vector model (large)
    * ```spacy download en_core_web_lg ```   
1. NodeJS, NPM and NeAt-Vision for attention weights visualization
   * https://github.com/cbaziotis/neat-vision
    
    
 

## 1. Figure from Tables & t-statistics
To generate figures for tables VI, VII and VIII, simply run the command 

``` python3 generate_officiel_figures.py ```

Items from the will be presented in the output

     
## 2. Training Process
### Train Models
Run the command: ``` python3 train_model.py ```

Use the following arguments in the CLI
* `-m <model>`:  use `HAN` for baseline model and `MLHAN` for proposed model
* `-r <seed>`: for a specific random starting point (default is `42`). 
* `-e <experiment-name>`: use `fullcorpus` for full experiment, `multilabel` for multi-label task on lower-level, `10pct`, `15pct`,`30pct`, for generability experiment at 10%, 15% and 30% of the train dataset, `xlearn` for transfer learning experiment 

Other training parameters are hardcoded

Examples:
- `python3 train.py -m han -e fullcorpus -r 1337`
- `python3 train.py -m mlhan -e fullcorpus -r 1337`
- `python3 train.py -m han -e 15pct -r 1203`
- `python3 train.py -m mlhan -e 15pct -r 1203`
- `python3 train.py -m mlhan -e xlearn -r 1234`

After the training process, attention weights will be produced.

### Evaluate Models
Run the command: ```python3 evaluate_models.py```

It will recursively evaluate all trained model and present the performance in the output

## 3. Obtain Risk Factors
Run the command: ```python3 get_risk_factors.py```

Use the same arguments used to train the model 
* `-m <model>`:  `HAN` or `MLHAN` 
* `-r <seed>`: the random starting point used
* `-e <experiment-name>`: use `fullcorpus`, `10pct`, `15pct`,`30pct` or `xlearn`  

Risk factor per intoxicant will be presented in the output

## 4. Visualizing Attention Weights
Launch NeAt-Vision `npm start`

Use the attention weights generated from the Training Process.